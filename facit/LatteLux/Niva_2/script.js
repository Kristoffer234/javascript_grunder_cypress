let coffeeItems = [];
const totalAmountElement = document.getElementById("total-amount");
const coffeeItemsContainer = document.getElementById("items-container");

fetch("data.json")
  .then((response) => response.json())
  .then((data) => {
    coffeeItems = data;
    coffeeItems
      .forEach((item) => {
        const coffeeItem = document.createElement("div");
        coffeeItem.className = "coffee-item";
        coffeeItem.setAttribute("data-testid", "coffee-item")
        coffeeItem.innerHTML = `
                  <span class="coffee-name" data-testid="coffee-name">${item.coffee}</span>
                  <span data-testid="coffee-price">${item.price} kr</span>
                  <div class="quantity-buttons" data-testid="quantity-buttons">
                  <button class="add-button" data-testid="add-button">+</button>
                  <span class="quantity" data-testid="quantity">0</span>
                  <button class="sub-button" data-testid="sub-button">-</button>`;
        coffeeItemsContainer.append(coffeeItem);

        const addButton = coffeeItem.querySelector(".add-button");
        const subButton = coffeeItem.querySelector(".sub-button");
        const quantityElement = coffeeItem.querySelector(".quantity");

        let quantity = 0;

        addButton.addEventListener("click", () => {
          quantity++;
          quantityElement.textContent = quantity;
          updateTotal();
        });

        subButton.addEventListener("click", () => {
          if (quantity > 0) {
            quantity--;
            quantityElement.textContent = quantity;
            updateTotal();
          }
        });
      })

    function updateTotal() {
      let totalAmount = 0;
      coffeeItems.forEach((item, index) => {
        const itemQuantity =
          coffeeItemsContainer.querySelectorAll(".quantity")[index].textContent;
        const itemPrice = item.price;
        totalAmount += itemQuantity * itemPrice;
      });
      totalAmountElement.textContent = totalAmount;
    }
  })
  .catch((err) => console.log("Error fetching data:", err));

