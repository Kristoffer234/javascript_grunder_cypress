
const ipBtn = document.getElementById("ip-btn")
const ip = document.getElementById("ip")
const ipCountry = document.getElementById("ip-country")
const ipCity = document.getElementById("ip-city")
const ipISP = document.getElementById("ip-isp")

const ipWeather = document.getElementById("ip-weather")
const map = document.getElementById("map")


ipBtn.addEventListener("click", () => {

    // Bygg klart resten :)
    const getIpInfo = ip => fetch(`http://ip-api.com/json/${ip}`)


    fetch("https://api.ipify.org/?format=json")
        .then(res => res.json())
        .then(data => {
            ip.innerText = data.ip
            getIpInfo(data.ip)
        })

})
